#ifndef __MAGICITE_SCENE_H__
#define __MAGICITE_SCENE_H__

#include "cocos2d.h"

class MagiciteScene : public cocos2d::Scene
{
public:
    virtual bool init();

    CREATE_FUNC(MagiciteScene);
};

#endif // __MAGICITE_SCENE_H__
