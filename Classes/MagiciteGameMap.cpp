#include "MagiciteGameMap.h"

USING_NS_CC;

MagiciteGameMap::MagiciteGameMap()
{

}

MagiciteGameMap::~MagiciteGameMap()
{

}

void MagiciteGameMap::MoveMap(int seed)
{
    if (seed < 0)
    {
        if (_BackGround->getPositionX() + seed <= 0)
        {
            _BackGround->setPositionX(_BackGround->getPositionX() - seed);
        }
    }
    else
    {
        if (_BackGround->getPositionX() + _BackGround->getContentSize().width >= _visibleSize.width)
        {
            _BackGround->setPositionX(_BackGround->getPositionX() - seed);
        }
    }
}

bool MagiciteGameMap::init()
{
    if (!Layer::init())
    {
        return false;
    }

    _visibleSize = Director::getInstance()->getVisibleSize();
    _BackGround = cocos2d::Sprite::create("ground.png");
    _BackGround->setAnchorPoint(Point::ZERO);
    this->addChild(_BackGround);
    return true;
}