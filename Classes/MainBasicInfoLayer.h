#ifndef __MAIN_BASIC_INFO_LAYER_H__
#define __MAIN_BASIC_INFO_LAYER_H__

#include "cocos2d.h"

class MainBasicInfoLayer : public cocos2d::Layer
{
public:
    virtual bool init();

    CREATE_FUNC(MainBasicInfoLayer);
};

#endif // __MAIN_BASIC_INFO_LAYER_H__
